﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SRMTMTEST.Model
{
    public class HistoricalUnitRelation
    {
        public int PrecedingUnitId { get; set; }
        public Unit PrecedingUnit { get; set; }
        public int SucceedingUnitId { get; set; }
        public Unit SucceedingUnit { get; set; }
    }
}
