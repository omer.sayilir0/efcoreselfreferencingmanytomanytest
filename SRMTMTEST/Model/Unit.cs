﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SRMTMTEST.Model
{
    public class Unit
    {
        public int UnitId { get; set; }
        public IList<HistoricalUnitRelation> Predecessors { get; set; }
        public IList<HistoricalUnitRelation> Successors { get; set; }
    }
}
