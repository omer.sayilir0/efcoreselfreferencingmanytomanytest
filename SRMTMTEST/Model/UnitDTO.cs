﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SRMTMTEST.Model
{
    public class UnitDTO
    {
        public int UnitId { get; set; }
        public int[] PredecessorIds { get; set; }
        public int[] SuccessorIds { get; set; }

    }
}
