﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SRMTMTEST.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SRMTMTEST
{

    public class TestDBContext : DbContext
    {
        private readonly IConfiguration Configuration;

        public TestDBContext(DbContextOptions options, IConfiguration configuration) : base(options)
        {
            Configuration = configuration;
        }

        public DbSet<Unit> Units { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql($"Host=192.168.6.130;Database=TestDb;Username=postgres;Password=postgres;Integrated Security=true;Pooling=true;");
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Unit>()
                 .HasKey(u => u.UnitId);

            builder.Entity<HistoricalUnitRelation>()
                .HasKey(hisur => new { hisur.PrecedingUnitId, hisur.SucceedingUnitId });

            builder.Entity<HistoricalUnitRelation>()
                .HasOne<Unit>(hisur => hisur.PrecedingUnit)
                .WithMany(u => u.Predecessors)
                .HasForeignKey(hisur => hisur.PrecedingUnitId)
                .OnDelete(DeleteBehavior.Restrict);


            builder.Entity<HistoricalUnitRelation>()
                .HasOne<Unit>(hisur => hisur.SucceedingUnit)
                .WithMany(u => u.Successors)
                .HasForeignKey(hisur => hisur.SucceedingUnitId);
        }



    }
}

