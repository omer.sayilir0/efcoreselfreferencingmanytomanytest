﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace SRMTMTEST.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    UnitId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.UnitId);
                });

            migrationBuilder.CreateTable(
                name: "HistoricalUnitRelation",
                columns: table => new
                {
                    PrecedingUnitId = table.Column<int>(nullable: false),
                    SucceedingUnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistoricalUnitRelation", x => new { x.PrecedingUnitId, x.SucceedingUnitId });
                    table.ForeignKey(
                        name: "FK_HistoricalUnitRelation_Units_PrecedingUnitId",
                        column: x => x.PrecedingUnitId,
                        principalTable: "Units",
                        principalColumn: "UnitId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HistoricalUnitRelation_Units_SucceedingUnitId",
                        column: x => x.SucceedingUnitId,
                        principalTable: "Units",
                        principalColumn: "UnitId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HistoricalUnitRelation_SucceedingUnitId",
                table: "HistoricalUnitRelation",
                column: "SucceedingUnitId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistoricalUnitRelation");

            migrationBuilder.DropTable(
                name: "Units");
        }
    }
}
