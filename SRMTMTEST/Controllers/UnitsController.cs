﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SRMTMTEST;
using SRMTMTEST.Model;

namespace SRMTMTEST.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnitsController : ControllerBase
    {
        private readonly TestDBContext _context;

        public UnitsController(TestDBContext context)
        {
            _context = context;
        }


        // GET: api/Units/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Unit>> GetUnit(int id)
        {
            var unit = await _context.Units.FindAsync(id);

            if (unit == null)
            {
                return NotFound();
            }

            return unit;
        }


        // POST: api/Units
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Unit>> PostUnit(UnitDTO unitDTO)
        {
            Unit unit = new Unit();
            if (unitDTO.PredecessorIds != null)
            {
                unit.Successors = new List<HistoricalUnitRelation>();
                unit.Predecessors = new List<HistoricalUnitRelation>();
                List<Unit> predecessorUnitObjects = _context.Units.Where(u => unitDTO.PredecessorIds.Contains(u.UnitId)).ToList();
                foreach (Unit predecessorUnitObject in predecessorUnitObjects)
                {
                    HistoricalUnitRelation historicalUnitRelation = new HistoricalUnitRelation() { PrecedingUnit = predecessorUnitObject, SucceedingUnit = unit };
                    unit.Predecessors.Add(historicalUnitRelation);

                    //test with adding relation from both directions

                    //if (predecessorUnitObject.Successors == null)
                    //{
                    //    predecessorUnitObject.Successors = new List<HistoricalUnitRelation>
                    //    {
                    //        historicalUnitRelation
                    //    };
                    //}
                    //else
                    //{
                    //    predecessorUnitObject.Successors.Add(historicalUnitRelation);
                    //}
                }
            }

            _context.Units.Add(unit);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUnit", new { id = unit.UnitId }, unit);
        }

        private bool UnitExists(int id)
        {
            return _context.Units.Any(e => e.UnitId == id);
        }
    }
}
